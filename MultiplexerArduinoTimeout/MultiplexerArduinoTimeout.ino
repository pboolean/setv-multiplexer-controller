/*
  MultiplexerArduino

  The Arduino Pro Mini is used to parse the 
  pulse train signal for an R/C servo and 
  output whether the motor is on or off.
*/

/*

  **** PINS ****
  Digital 2 - input data line from Servo (White)
  GND - ground from Servo (Black)
  RAW - Servo power line (Red)
  
  Digital 13 - digital output to multiplexer (Yellow)
  GND - Ground reference for multiplxer
*/

#define OFF_PULSE_WIDTH 1000 // microseconds
#define ON_PULSE_WIDTH 2000 // microseconds
#define PULSE_WIDTH_TIMEOUT 20000 // microseconds

#define MICROS_RESOLUTION 48 // could be off by +/- 24 us

#define PULSE_INPUT_PIN 2 // digital 2 allows interrupts
#define OUTPUT_PIN 13 // output to MUX

#define MOD_VAL 134217728 // 2^27

unsigned int risingEdgeMicros = 0;

volatile boolean didChange = false;
volatile boolean didRise = false;

void pulseChangeISR()
{
  didChange = true;
}

void risingEdge()
{
  risingEdgeMicros = micros();
}


/**
 * Determine whether a number is within <error> of a target number.
 */
boolean range(int num, int target, int error)
{
  return ((num > (target - error)) && (num < (target + error)));
}

/**
 * Helper function to calculate the width of a detected pulse, taking into
 * account that the timestamp might have rolled over.
 */
int findPulseWidth(unsigned int risingEdge, unsigned int fallingEdge)
{
  int pulseWidth = fallingEdge - risingEdge;
  
  // if the counter overflows (happens after ~ 70 minutes)
  if (fallingEdge < risingEdge)
  {
    pulseWidth = (fallingEdge + MOD_VAL) - (risingEdge % MOD_VAL);
  }
  
  return pulseWidth;
}

void fallingEdge()
{
  unsigned int fallingEdgeMicros = micros();
  int pulseWidth = findPulseWidth(risingEdgeMicros, fallingEdgeMicros);
  
  if(range(pulseWidth, ON_PULSE_WIDTH, MICROS_RESOLUTION))
  {
    digitalWrite(OUTPUT_PIN, HIGH);
  } 
  else if(range(pulseWidth, OFF_PULSE_WIDTH, MICROS_RESOLUTION))
  {
    digitalWrite(OUTPUT_PIN, LOW);
  } 
  else
  {
    digitalWrite(OUTPUT_PIN, LOW);
    Serial.print("INVALID PULSE WIDTH: ");
    Serial.println(pulseWidth);
  }
}

void setup()
{
  Serial.begin(115200);
  Serial.println("starting");

  // pins
  pinMode(PULSE_INPUT_PIN, INPUT);
  pinMode(OUTPUT_PIN, OUTPUT);
  
  digitalWrite(OUTPUT_PIN, LOW); // start off in off state
  
  attachInterrupt(0, pulseChangeISR, CHANGE); // interrupt 0 is on digital pin 2
}

void loop() 
{
  while(!didChange)
  {
    // watch for the pulse train stopping
    if(findPulseWidth(risingEdgeMicros, micros()) > PULSE_WIDTH_TIMEOUT)
    {
      digitalWrite(OUTPUT_PIN, LOW);
    }
  }
  
  didChange = false;
  boolean inputLineIsHigh = digitalRead(PULSE_INPUT_PIN);
  if (inputLineIsHigh)
  {
    risingEdge();
  } else
  {
    fallingEdge();
  }
}
